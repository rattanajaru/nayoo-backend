<?php

namespace App\Http\Controllers;

use App\SearchLog;
use Illuminate\Http\Request;

class SearchLogController extends Controller
{
    public function index()
    {
        return SearchLog::all();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $item = SearchLog::find($request->id);
        if($item !== null) {
            $item->update([
                'location'  => $request->location,
            ]);
            return SearchLog::find($request->id);
        } else {
            $item = SearchLog::create([
                'location'  => $request->location,
            ]);
            return SearchLog::find($request->id);
        }
    }

    public function show($id)
    {
        return SearchLog::find($id);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $item = SearchLog::find($item);
        return $item->update([
            'location'  => $request->location,
        ]);
    }

    public function destroy($id)
    {
        return SearchLog::destroy($id);
    }
}
